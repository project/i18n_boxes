Contents of this file:

1. Description
2. Installation and configuration
3. Usage
4. Troubleshooting


Description
-----------

The i18n (Internationalization) module supports translation of blocks from
Drupal core with i18n_block, however core blocks cannot be exported to code.
This module enables block titles and block content to be exported to code (via
the Boxes module) and then be translated (via i18n_string included with
the i18n module).


Installation and Configuration
------------------------------

Note, this module requires locale, block, boxes, i18n, i18n_string, and the
ctools modules.

1. Download and unzip/untar the module into your Drupal site installation.

2. Ensure that the core block module is enabled before enabling i18n_boxes.

3. Enable the "Boxes translation" module, which should appear within the
Multilanguage section of the Module administration page.

4. Ensure the text formats you will be using for the Boxes body text are
translatable by visiting the "Strings" tab of the Multilingual settings page at
Administration > Configuration > Regional and language > Multilingual settings,
and checking the checkbox for the appropriate text formats.

5. Visit the Admin > Structure > Blocks page.  You will see a new option link
next to the "+ Add block" link called "+ Add translatable box".


Usage
-----

1. This module adds a "+ Add translatable box" link to the Block administration
page at Admin > Structure > Blocks.

2. After you add a translatable box, you're given the option to select the
Multilingual settings for the content. Select "All languages (Translatable)",
and specify the Box machine name, the administrative title, the Box title, and
the Box body -- all these should be in the site's *default language*.

3. Save the box, and enable it for display, by placing it into a region on the
Block administration UI (or Panel, if you use panels module).

4. Translating boxes.

There are two ways to translate boxes, via the translation interface in the
Drupal administrative section provided by i18n_strings module, or (optionally)
via the on-screen translation provided by the l10n_client module.

4.a) Translating boxes via i18n_strings

Visit the "Translate" tab of the "Translate interface" page at Administration >
Configuration > Regional and language > Translate interface.  Search for the
text strings you just created for the box title and box body by specifying them
in the search box. Translate them using the interface provided, for each
additional language you have enabled.

Visit the translated version of the page containing the translatable box to
verify that the string was translated. The method to change interface language
varies depending on how the locale module is configured for language detection
on your site. Refer to the locale module documentation for further configuration
information.

4.b) Translating boxes using the on-screen l10n_client.

Ensure the l10n_client module is enabled and configured properly, and that you
have permission to see the blue translation bar on the site's front-end.

Visit any page that contains the translatable box you created and enabled in
steps 2 and 3 above. Expand the l10n_client blue bar at the bottom of the page
and search for the text you wish to translate.  Specify the text and save.

5. Exporting boxes.

The default language version of each translatable box may be exported to php
code, typically using the Features module.  You may also wish to export the
translated versions from the database to .po files using the I18n module's
export feature.

Troubleshooting
---------------

Problem: Missing "+ Add translatable box" link on Admin > Structure > Blocks.

Solution: In certain circumstances, is possible that the installation script gets run
before the block module (a dependency of this module) has been enabled. This
will cause the "+ Add translatable box" link to disappear.  Disable and reenable
this module to force the module weight to be higher than that of the block
module, which should remedy the problem.


Problem: The string boxes:i18n:machine_name:body for textgroup boxes is not
allowed for translation because of its text format.

Solution: You need to make your text formats translatable in the Multilingual
settings.  Go to the "Strings" tab of the Administration > Configuration >
Regional and language > Multilingual settings, and check the checkbox for the
input format(s) you intend to make translatable.
